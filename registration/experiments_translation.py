import os
import sys
import copy
import open3d
import torch
import torch.utils.data
import logging
import numpy as np
from model import PointNet, Decoder, SolveRegistration
import se_math.transforms as transforms

LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

res_y_1 = []
res_y_2 = []
res_x = []

# visualize the point clouds
def draw_registration_result(source, target, transformation):
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([0, 0.651, 0.929])
    open3d.visualization.draw_geometries([source_temp, target_temp])
    source_temp.transform(transformation)
    open3d.visualization.draw_geometries([source_temp, target_temp])

def output_transformation_matrix(source, target, transformation, approach):
    evaluation = open3d.pipelines.registration.evaluate_registration(
        source, target, 100, transformation)
    print("fitness", evaluation.fitness)
    print("rmse", evaluation.inlier_rmse)
    print(transformation)

    if approach == 1:
        res_y_1.append(evaluation.fitness)
    else:
        res_y_2.append(evaluation.fitness)

class Demo:
    def __init__(self):
        self.dim_k = 1024
        self.max_iter = 10  # max iteration time for IC algorithm
        self._loss_type = 1  # see. self.compute_loss()

    def create_model(self):
        # Encoder network: extract feature for every point. Nx1024
        ptnet = PointNet(dim_k=self.dim_k)
        # Decoder network: decode the feature into points, not used during the evaluation
        decoder = Decoder()
        # feature-metric ergistration (fmr) algorithm: estimate the transformation T
        fmr_solver = SolveRegistration(ptnet, decoder, isTest=True)
        return fmr_solver

    def evaluate(self, solver, p0, p1, device):
        solver.eval()
        with torch.no_grad():
            p0 = torch.tensor(p0,dtype=torch.float).to(device)  # template (1, N, 3)
            p1 = torch.tensor(p1,dtype=torch.float).to(device)  # source (1, M, 3)
            solver.estimate_t(p0, p1, self.max_iter)

            est_g = solver.g  # (1, 4, 4)
            g_hat = est_g.cpu().contiguous().view(4, 4)  # --> [1, 4, 4]

            return g_hat

def ground_truth_main(source, target, transformation):

    output_transformation_matrix(source, target, transformation.numpy())
    #draw_registration_result(source, target, transformation.numpy())

def icp_main(downpcd0, downpcd1):

    threshold = 200
    trans_init = np.asarray([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]])

    target = downpcd0
    source = downpcd1

    # Apply point-to-point ICP
    reg_p2p = open3d.pipelines.registration.registration_icp(
        source, target, threshold, trans_init,
        open3d.pipelines.registration.TransformationEstimationPointToPoint())

    output_transformation_matrix(source, target, reg_p2p.transformation, 2)
    #draw_registration_result(source, target, reg_p2p.transformation)

def fmr_main(p0, p1, p0_pcd, p1_pcd):
    fmr = Demo()
    model = fmr.create_model()
    pretrained_path = "./result/fmr_model_7scene.pth"
    model.load_state_dict(torch.load(pretrained_path, map_location='cpu'))

    device = "cpu"
    model.to(device)
    T_est = fmr.evaluate(model, p0, p1, device)

    output_transformation_matrix(p1_pcd, p0_pcd, T_est.numpy(), 1)
    #draw_registration_result(p1_pcd, p0_pcd, T_est.numpy())

def rotate(p0_src, p0):

    # generate rotation

    for i in range(0, 90, 10):
        res_x.append(i)

        p1_src = copy.deepcopy(p0_src)
        p1_src.rotate(p0_src.get_rotation_matrix_from_xyz((np.pi / 180 * i, 0, 0)))
        downpcd1 = p1_src.voxel_down_sample(voxel_size=50)
        p1 = np.asarray(downpcd1.points)
        p1 = np.expand_dims(p1, 0)

        # feature metric registration
        fmr_main(p0, p1, downpcd0, downpcd1)

        # icp registration
        icp_main(downpcd0, downpcd1)

def translate(p0_src, p0):

    # generate Translation

    for i in range(0, 3000, 300):
        res_x.append(i)

        p1_src = copy.deepcopy(p0_src).translate((0, i, 0))
        downpcd1 = p1_src.voxel_down_sample(voxel_size=50)
        p1 = np.asarray(downpcd1.points)
        p1 = np.expand_dims(p1, 0)

        # feature metric registration
        fmr_main(p0, p1, downpcd0, downpcd1)

        # icp registration
        icp_main(downpcd0, downpcd1)


if __name__ == '__main__':
    path0 = "../../data/livingroom-9.ply"

    p0_src = open3d.io.read_point_cloud(path0)
    downpcd0 = p0_src.voxel_down_sample(voxel_size=50) # 0.05 50
    p0 = np.asarray(downpcd0.points)
    p0 = np.expand_dims(p0,0)

    np.set_printoptions(precision=6)

    translate(p0_src, p0)
    
    print("Variation:", res_x)
    print("Result of FMR:", res_y_1)
    print("Result of ICP:", res_y_2)
        
    
