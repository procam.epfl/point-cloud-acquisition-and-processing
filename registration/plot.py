import matplotlib.pyplot as plt
import numpy as np

A_x = np.array([0, 300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700]
)

A_y = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
)

B_x = np.array([0, 300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700]
)

B_y = np.array([1.0, 1.0, 1.0, 0.5078137717727494, 0.19322806446361712, 0.18989093276900537, 0.13372944815236856, 0.1211134624776168, 0.0, 0.0]
)

plt.plot (A_x,A_y, label="Feature-Metric Registration")
plt.scatter (A_x, A_y)
plt.plot (B_x,B_y, label="ICP")
plt.scatter (B_x, B_y)

# plt.xlabel('Rotation Degree [°]')
plt.xlabel('Translation Distance [mm]')
plt.ylabel('Fitness')
plt.title('Registration Evaluation')
plt.legend()
# plt.show()
plt.savefig('t-summary.png')
