# Point Cloud Acquisition and Registration

This is the implementation of the semester project. The point cloud acquisition pipeline is modified from [the repository](https://github.com/bponsler/kinectToPly) and is provided in the `acquisition` folder. The registration code is modified from [the repository](https://github.com/XiaoshuiHuang/fmr) and is provided in the `registration` folder.

### Installation

To install most of the packages, you can simply run

```bash
pip3 install -r requirement.txt
```

on Raspberry Pi. Note that some packages (Open3D) do not support pip install on Arm architecture, you should build the package by yourself. This [issue](https://github.com/isl-org/Open3D/issues/2895) provides the solution for installing Open3D on Raspberry Pi.

To install the libfreenect package, you can follow the steps below.

1. Run `sudo apt-get install freenect`
2. Git clone [libfreenect](https://github.com/OpenKinect/libfreenect)
3. Follow installation guide

Be sure you build the python wrapper before executing make command.

### Acquisition

To execute the acquisition pipeline, you can directly run

```bash
python3 kinect_to_ply.py /path/to/target/folder/filename
```

The generated ply file will be saved to the target path.

### Registration

To recover the experimental result of registration, modify the `path0` variable to the point cloud you want to use. Then, execute

```bash
python3 experiments_rotation.py
python3 experiments_translation.py
```

To visualize the result, uncomment `    draw_registration_result(source, target, reg_p2p.transformation)` and  `draw_registration_result(p1_pcd, p0_pcd, T_est.numpy())` in icp_main() and fmr_main() functions.
